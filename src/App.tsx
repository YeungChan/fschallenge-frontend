import React from "react";
import logo from "./logo.svg";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Home from "./pages/Home";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import AddTeamMember from "./pages/AddTeamMember";
import AddProject from "./pages/AddProject";
import axios from "axios";
axios.defaults.baseURL="http://localhost:8081"

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home></Home>}></Route>
          <Route path="/addTeamMember" element={<AddTeamMember></AddTeamMember>}></Route>
          <Route path="/addProject" element={<AddProject></AddProject>}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
