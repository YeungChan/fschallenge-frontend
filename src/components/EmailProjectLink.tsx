import { ChangeEvent, useEffect, useState } from "react";
import { Button, Form, Stack } from "react-bootstrap";
import { getAllProjects, postLinkEmailWithProject } from "../rest/RestClient";
import { EmailProject } from "../types/EmailProject";
import { Project } from "../types/Project";

function EmailProjectLink({
  setMessage,
  refetch,
}: {
  setMessage: (message: string) => void;
  refetch: () => void;
}) {
  const [link, setLink] = useState<EmailProject>({
    email: "",
    projectName: "",
  });
  const [projects, setProjects] = useState<Project[]>([]);

  useEffect(() => {
    async function init() {
      const { data } = await getAllProjects();
      setProjects(data);
      if (data.length > 0) {
        setLink({ ...link, projectName: data[0].name });
      }
    }
    init();
  }, []);

  function onClickLink() {
    setLink({...link, email:""})
    postLinkEmailWithProject(link)
      .then((response) => {
        setMessage(response.data);
        refetch();
      })
      .catch((error) => setMessage(error.response.data));
  }

  function onChangeLinkForm(
    e: ChangeEvent<HTMLTextAreaElement | HTMLInputElement | HTMLSelectElement>,
    field: string
  ) {
    setLink({ ...link, [field]: e.target.value });
    console.log(e.target.value);
    console.log(field);
  }
  return (
    <Stack
      className="mx-auto w-50"
      direction="horizontal"
      gap={2}
    >
      <Form.Control
      className="w-70"
        type="email"
        value={link.email}
        placeholder="Enter an email"
        onChange={(e: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) =>
          onChangeLinkForm(e, "email")
        }
      />
      <Form.Select
        onChange={(e: React.ChangeEvent<HTMLSelectElement>) =>
          onChangeLinkForm(e, "projectName")
        }
      >
        {projects.length === 0 ? (
          <option>Geen projecten</option>
        ) : (
          projects.map((project: Project, i: number) => (
            <option key={i} value={project.name}>
              {project.name}
            </option>
          ))
        )}
      </Form.Select>
      <Button
        disabled={projects.length === 0 ? true : false}
        onClick={onClickLink}
      >
        Link
      </Button>
    </Stack>
  );
}
export default EmailProjectLink;
