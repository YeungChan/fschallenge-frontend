import { ListGroup } from "react-bootstrap";
import { TeamMember } from "../types/TeamMember";
import UsersProjectsListItem from "./UsersProjectsListItem";

function UsersProjectsList({ teamMembers }: { teamMembers: TeamMember[] }) {
  return (
    <div className="mx-auto w-50 mh-75 d-flex flex-column justify-content-start overflow-auto mt-3">
      <span className="w-100 text-start border-bottom border-dark">
        TEAM MEMBERS + PROJECTS
      </span>
      <ListGroup>
        {teamMembers.map((teamMember: TeamMember, i: number) => (
          <UsersProjectsListItem key={i} teamMember={teamMember} />
        ))}
      </ListGroup>
    </div>
  );
}
export default UsersProjectsList;
