import { ListGroup } from "react-bootstrap";
import Image from "react-bootstrap/Image";
import { Project } from "../types/Project";
import { TeamMember } from "../types/TeamMember";

function UsersProjectsListItem({ teamMember }: { teamMember: TeamMember }) {
  return (
    <ListGroup.Item className="w-100 border-0 border-bottom border-dark d-flex justify-content-start">
      <Image
        roundedCircle
        src={teamMember.pictureUrl}
        alt="Profile picture"
        style={{ width: "50px", height: "50px", marginRight: "0.5rem" }}
      ></Image>
      <div className="d-flex flex-column">
        <span className="text-start" style={{  fontWeight: "bold" }}>
          {teamMember.name}
        </span>
        <span className="text-start text-secondary">
          {teamMember.role}
        </span>
      </div>
      {typeof teamMember.projects !== "undefined" && (
        <div
        className="d-flex align-items-center"
          style={{ marginLeft: "auto" }}
        >
          {teamMember.projects.map((project: Project, i: number) => (
            <span
              key={i}
              className="px-2 mx-1"
              style={{
                color: project.fontColor,
                backgroundColor: project.backgroundColor,
                borderRadius: "10px",
              }}
            >
              {project.name}
            </span>
          ))}
        </div>
      )}
    </ListGroup.Item>
  );
}
export default UsersProjectsListItem;
