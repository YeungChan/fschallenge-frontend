import { ChangeEvent } from "react";
import { Form } from "react-bootstrap";

function FormInput({
  label,
  value,
  name,
  type,
  onChange
}: {
  label: string;
  value: string;
  name: string;
  type?:string;
  onChange:(e: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>, field:string)=>void
}) {
  return (
    <>
      <Form.Label className="text-start" htmlFor={name}>
        {label}
      </Form.Label>
      <Form.Control
        type={typeof type==="undefined"?"text":type}
        value={value}
        id={name}
        onChange={(e: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) =>
          onChange(e, name)
        }
      />
    </>
  );
}
export default FormInput;
