import { ChangeEvent, useState } from "react";
import { Alert, Button, Form } from "react-bootstrap";
import FormInput from "../components/FormInput";
import { postAddTeamMember } from "../rest/RestClient";
import { TeamMember } from "../types/TeamMember";

function AddTeamMember() {
  const [message, setMessage] = useState<string>("");

  const [teamMember, setTeamMember] = useState<TeamMember>({
    name: "",
    email: "",
    pictureUrl: "",
    role: "",
  });

  function onChangeTeamMemberForm(
    e: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>,
    field: string
  ) {
    setTeamMember({ ...teamMember, [field]: e.target.value });
  }

  function onSubmitTeamMember() {
    setTeamMember({ name: "", email: "", pictureUrl: "", role: "" });
    postAddTeamMember(teamMember)
      .then((response) => setMessage(response.data))
      .catch((error) => setMessage(error.response.data));
  }

  return (
    <div
      className="d-flex flex-column justify-content-center"
      style={{
        height: "100vh",
      }}
    >
      {message !== "" && (
        <Alert
          className="w-50 mx-auto d-flex flex-direction-row justify-content-between"
          variant="primary"
        >
          {message}
          <Button onClick={() => setMessage("")} variant="outline-success">
            close
          </Button>
        </Alert>
      )}
      <div
        className="mx-auto p-3 mt-5 d-flex flex-column shadow-lg p-3 mb-5 bg-white"
        style={{
          minWidth: "30%",
          height: "fit-content",
          borderRadius: "2rem",
        }}
      >
        <h1 style={{ fontSize: "1.5rem" }}>Add team member</h1>

        <FormInput
          label="Name"
          name="name"
          value={teamMember.name}
          onChange={onChangeTeamMemberForm}
        ></FormInput>

        <FormInput
          label="Email"
          name="email"
          value={teamMember.email}
          onChange={onChangeTeamMemberForm}
        ></FormInput>

        <FormInput
          label="Role"
          name="role"
          value={teamMember.role}
          onChange={onChangeTeamMemberForm}
        ></FormInput>

        <FormInput
          label="Picture url"
          name="pictureUrl"
          value={teamMember.pictureUrl}
          onChange={onChangeTeamMemberForm}
        ></FormInput>

        <Button
          className="mx-auto mt-2"
          style={{ width: "fit-content" }}
          onClick={onSubmitTeamMember}
        >
          Submit
        </Button>
      </div>
    </div>
  );
}
export default AddTeamMember;
