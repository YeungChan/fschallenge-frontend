import { useEffect, useState } from "react";
import { Alert, Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import EmailProjectLink from "../components/EmailProjectLink";
import UsersProjectsList from "../components/UsersProjectsList";
import { getAllTeamMembers } from "../rest/RestClient";
import { TeamMember } from "../types/TeamMember";
function Home() {
  const navigate = useNavigate();
  const [message, setMessage] = useState<string>("");
  const [teamMembers, setTeamMembers] = useState<TeamMember[]>([]);

  useEffect(() => {
    fetchTeamMembers();
  }, []);

  async function fetchTeamMembers() {
    const { data } = await getAllTeamMembers();
    setTeamMembers(data);
  }
  return (
    <div style={{ width: "100vw", height: "100vh" }}>
      {message !== "" && (
        <Alert
          className="w-50 mx-auto d-flex flex-direction-row justify-content-between"
          variant="primary"
        >
          {message}
          <Button onClick={() => setMessage("")} variant="outline-success">
            close
          </Button>
        </Alert>
      )}
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          marginBottom: "1rem",
        }}
      >
        <img
          style={{ maxHeight: "4rem" }}
          src="/images/groupIcon.svg"
          alt="Group icon"
        ></img>
        <h1 style={{ fontWeight: "bold", fontSize: "1.5rem" }}>
          Add projects to team members
        </h1>
      </div>
      <EmailProjectLink setMessage={setMessage} refetch={fetchTeamMembers} />
      <div
        className="mx-auto mt-3"
        style={{ display: "flex", justifyContent: "center" }}
      >
        <Button
          style={{ marginRight: "1rem" }}
          onClick={() => navigate("/addTeamMember")}
        >
          Add team member
        </Button>
        <Button onClick={() => navigate("/addProject")}>Add project</Button>
      </div>
      <UsersProjectsList teamMembers={teamMembers} />
    </div>
  );
}
export default Home;
