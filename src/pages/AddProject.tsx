import { ChangeEvent, useState } from "react";
import { Alert, Button, Form } from "react-bootstrap";
import FormInput from "../components/FormInput";
import { postAddProject } from "../rest/RestClient";
import { Project } from "../types/Project";

function AddProject() {
  const [message, setMessage] = useState<string>("");
  const [project, setProject] = useState<Project>({
    name: "",
    backgroundColor: "#FF0000",
    fontColor: "#FFFFFF",
  });

  function onChangeProjectForm(
    e: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>,
    field: string
  ) {
    setProject({ ...project, [field]: e.target.value });
  }

  function onSubmitProject() {
    setProject({ name: "", backgroundColor: "#FF0000", fontColor: "#FFFFFF" });
    postAddProject(project)
      .then((response) => setMessage(response.data))
      .catch((error) => setMessage(error.response.data));
  }

  return (
    <div
      className="d-flex flex-column justify-content-center"
      style={{ height: "100vh" }}
    >
      {message !== "" && (
        <Alert
          className="w-50 mx-auto d-flex flex-direction-row justify-content-between"
          variant="primary"
        >
          {message}
          <Button onClick={() => setMessage("")} variant="outline-success">
            close
          </Button>
        </Alert>
      )}
      <div
        className="mx-auto p-3 mt-5 d-flex flex-column shadow-lg p-3 mb-5 bg-white"
        style={{
          borderRadius: "2rem",
        }}
      >
        <h1 style={{ fontSize: "1.5rem" }}>Add project</h1>
        <FormInput
          label="Name project"
          name="name"
          value={project.name}
          onChange={onChangeProjectForm}
        ></FormInput>

        <FormInput
          label="Background color tag"
          name="backgroundColor"
          value={project.backgroundColor}
          onChange={onChangeProjectForm}
          type="color"
        ></FormInput>

        <FormInput
          label="Font color tag"
          name="fontColor"
          value={project.fontColor}
          onChange={onChangeProjectForm}
          type="color"
        ></FormInput>

        <Button
          className="mx-auto mt-2"
          style={{ width: "fit-content" }}
          onClick={onSubmitProject}
        >
          Submit
        </Button>
      </div>
    </div>
  );
}
export default AddProject;
