import axios from "axios";
import { EmailProject } from "../types/EmailProject";
import { Project } from "../types/Project";
import { TeamMember } from "../types/TeamMember";

export async function getAllProjects() {
  return axios.get("/projects/all");
}

export async function getAllTeamMembers() {
  return axios.get("/teamMembers/all");
}

export function postLinkEmailWithProject(link: EmailProject) {
  const { email, projectName } = link;
  return axios.post("/teamMembers/link", {
    email: email,
    projectName: projectName,
  });
}

export function postAddProject(project: Project) {
  const { name, backgroundColor, fontColor } = project;
  return axios.post("/projects/add", {
    name: name,
    backgroundColor: backgroundColor,
    fontColor: fontColor,
  });
}

export function postAddTeamMember(teamMember: TeamMember) {
  const { email, name, role, pictureUrl } = teamMember;
  return axios.post("/teamMembers/add", {
    name: name,
    email: email,
    role: role,
    pictureUrl: pictureUrl,
  });
}
