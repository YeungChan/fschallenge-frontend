import { Project } from "./Project";

export type TeamMember = {
  name: string;
  email: string;
  role: string;
  pictureUrl: string;
  projects?: Project[];
};
