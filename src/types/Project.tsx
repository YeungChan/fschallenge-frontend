import { TeamMember } from "./TeamMember";

export type Project = {
  name: string;
  backgroundColor: string;
  fontColor: string;
  teamMembers?: TeamMember[];
}
